#include "File.h"

vector<fs::path> fileList(char * directory)
{
	vector<fs::path> file;
	fs::path k(directory);
	if (is_directory(k))
	{
		fs::directory_iterator i(k), e; fs::end(e);
		for ( ; i!= e; i++)
		{
			if (!is_directory(*i))
			{
				file.push_back(*i);
			}
		}
	}
	return file;
}

vector<CharStat> CharFreq(vector<fs::path>& p)
{
	vector<CharStat> table;
	unsigned int *stat= new unsigned int[256];
	for (int i = 0; i < 256; i++)
		stat[i] = 0;
	int n = p.size();
	for (int i = 0; i < n; i++)
	{
		ifstream temp(p[i].string());
		while (!temp.eof())
		{
			char tempChar = temp.get();
			stat[tempChar]++;
		}
		temp.close();
	}
	for (int i = 0; i < 256; i++)
	{
		if (stat[i]>0)
		{
			CharStat s;
			s.name = i; s.frequency = stat[i];
			table.push_back(s);
		}
	}
	//delete []stat;
	return table;
}
