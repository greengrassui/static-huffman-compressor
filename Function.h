#ifndef __FUNCTION_H__
#define __FUNCTION_H__

#include "File.h"
#include "HUFFile.h"
#include "HuffmanTree.h"
#include <iostream>
#include <fstream>
using namespace std;

void Encode(char *folder_in, char* file_out);

void View(char* file);

bool Decode(char* func, char* file);

#endif