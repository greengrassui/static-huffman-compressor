#ifndef __HUFFMANTREE_H__
#define __HUFFMANTREE_H__

#include "File.h"

struct HuffNode
{

};

HuffNode* HuffmanTree(vector<CharStat> stat);

unsigned int CompressFile(ifstream& file, ofstream& fileCompress, HuffNode* root, char& Align);

#endif
