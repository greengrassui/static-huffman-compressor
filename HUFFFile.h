#ifndef __HUFFILE_H__
#define __HUFFILE_H__

#include <string>
#include <iostream>
#include <vector>
#include "File.h"
using namespace std;

struct HUFHeader
{
	string fileName;
	char   alignBits;
	unsigned int sizeBefore;
	unsigned int sizeAfter;
	unsigned int curFileAddrOffset;
	unsigned int nextFileAddrOffset;
	bool Input(istream& in);
	bool Output(ostream& out);
};

//istream& operator>>(istream& in, HUFHeader& header);
//ostream& operator<<(ostream& out, HUFHeader& header);

class HUF
{
	char              m_CodeFile[2];
	unsigned char     m_nCode;
	vector<CharStat>  m_CodeTable;
	vector<HUFHeader> m_FileHeader;

public:
	HUF(void);
	HUF(char* Code, unsigned char nCode, vector<CharStat> CodeTable);
	~HUF(void);
	
	bool isHUF();
	HUFHeader getHeader(int i);
	void insertHeader(HUFHeader t);

	vector<HUFHeader> getHeaders() { return m_FileHeader; }

	friend istream& operator>>(istream& in, HUF& info);
	friend ostream& operator<<(ostream& out, HUF& info);
};

#endif
