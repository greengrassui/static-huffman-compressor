﻿/**************************************************
Thư viện này chứa các hàm dùng để thao tác với một 
dãy 256 bits tạo thành từ 32 unsigned char.
Tại sao lại là 256? Vì mã bit dài nhất của ký tự 
ít xuất hiện nhất có độ dài 256 bit.
**************************************************/

#include <limits.h>
#include <string.h>

#define NUM_BITS    256	/* Số bit của mỗi block */
#define NUM_BYTES   32  /* Số byte của mỗi block */    
#define LAST_BYTE   31  /* Byte cuối cùng trong block */

/* Mô tả chi tiết mỗi hàm vui lòng xem trong file .cpp */
void clearBlock(unsigned char *bits);
int testBit(const unsigned char *bits, unsigned char bit);
void leftShift(unsigned char *bits, int shifts);
void rightShift(unsigned char *bits, int shifts);
void copyBlock(unsigned char *dest, const unsigned char *src);