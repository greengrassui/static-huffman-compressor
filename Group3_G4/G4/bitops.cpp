﻿#include "bitops.h"

/***************************************************************
*	Tên hàm		:	clearBlock
*	Chức năng	:	clear toàn bộ bit trong một block (tức là
*					reset tất cả bit về 0). Lưu ý: hàm có sử
*					dụng memset.
*	Truyền vào	:	con trỏ tới mảng chứa các bit
*	Trả về		:	không
*	Người viết	:	Duy
***************************************************************/
void clearBlock(unsigned char *bits) {
	memset((void *)bits, 0, NUM_BYTES);
}

/***************************************************************
*	Tên hàm		:	testBit
*	Chức năng	:	trả về giá trị của một bit trong block
*	Truyền vào	:	con trỏ tới mảng chứa các bit, vị trí bit cần
*					biết giá trị
*	Trả về		:	giá trị của bit được chỉ định
*	Người viết	:	Duy
***************************************************************/
int testBit(const unsigned char *bits, unsigned char idx) {
	int byte;
	unsigned char mask;
	byte = idx / 8;        
	idx = idx % 8;          
	mask = (0x80 >> idx);
	return((bits[byte] & mask) != 0);
}

/***************************************************************
*	Tên hàm		:	leftShift
*	Chức năng	:	shift một khoảng cách về bên trái
*	Truyền vào	:	con trỏ tới mảng chứa các bit, khoảng cách cần
*					shift (tính bằng bit)
*	Trả về		:	không
*	Người viết	:	Duy
***************************************************************/
void leftShift(unsigned char *bits, int shifts) {
	int i;
	unsigned int overflow;
	int bytes = shifts / 8;     
	shifts = shifts % 8;        
	/* Shift số lần chia hết cho byte trước */
	if (bytes > 0) {
		for (i = 0; (i + bytes) < NUM_BYTES; i++) {
			bits[i] = bits[i + bytes];
		}

		for (i = NUM_BYTES; bytes > 0; bytes--) {
			bits[i - bytes] = 0;
		}
	}
	/* Sau đó shift phần còn lại */
	if (shifts > 0) {
		bits[0] <<= shifts;

		for (i = 1; i < NUM_BYTES; i++) {
			overflow = bits[i];
			overflow <<= shifts;
			bits[i] = (unsigned char)overflow;
			/* Số lần shift vượt giới hạn byte */
			if (overflow & 0xFF00) {
				bits[i - 1] |= (unsigned char)(overflow >> 8);
			}
		}
	}
}

/***************************************************************
*	Tên hàm		:	rightShift
*	Chức năng	:	shift một khoảng cách về bên phải
*	Truyền vào	:	con trỏ tới mảng chứa các bit, khoảng cách cần
*					shift (tính bằng bit)
*	Trả về		:	không
*	Người viết	:	Duy
***************************************************************/
void rightShift(unsigned char *bits, int shifts) {
	int i;
	unsigned int overflow;
	int bytes = shifts / 8;  
	shifts = shifts % 8;       
	/* Shift số lần chia hết cho byte trước */
	if (bytes > 0) {
		for (i = LAST_BYTE; (i - bytes) >= 0; i--) {
			bits[i] = bits[i - bytes];
		}

		while (bytes > 0) {
			bits[bytes - 1] = 0;
			bytes--;
		}
	}
	/* Sau đó shift phần còn lại */
	if (shifts > 0) {
		bits[LAST_BYTE] >>= shifts;

		for (i = LAST_BYTE - 1; i >= 0; i--) {
			overflow = bits[i];
			overflow <<= (8 - shifts);
			bits[i] = (unsigned char)(overflow >> 8);
			/* Số lần shift vượt giới hạn byte */
			if (overflow & 0xFF) {
				bits[i + 1] |= (unsigned char)overflow;
			}
		}
	}
}

/***************************************************************
*	Tên hàm		:	copyBlock
*	Chức năng	:	copy một block tới block khác
*	Truyền vào	:	con trỏ tới mảng chứa các bit cần copy, 
*					con trỏ tới mảng bit đích
*	Trả về		:	không
*	Người viết	:	Duy
***************************************************************/
void copyBlock(unsigned char *dest, const unsigned char *src) {
	memcpy((void *)dest, (void *)src, NUM_BYTES);
}