﻿#include "huffman.h"

/***************************************************************
*	Tên hàm		:	HuffmanTree
*	Chức năng	:	dựa vào bảng tần suất ký tự để tạo cây Huffman
*	Truyền vào	:	bắt buộc là bảng thống kê đã sắp xếp trọng số 
					(nếu trọng số bằng nhau sẽ ưu tiên ký tự có mã
					ASCII nhỏ hơn nằm trước)
*	Trả về		:	cây Huffman với root ở vị trí đầu tiên i = 0
					các phần tử lá sẽ có nLeft và nRight bằng -1
*	Người viết	:	Đạt
***************************************************************/
HUFFNode* HuffmanTree(vector<CharStat> stat)
{
	//tạo cây Huffman
	HUFFNode *HuffTree = new HUFFNode[stat.size() * 2 - 1];

	int Offset = 1, i, j;
	bool isAdded;
	int Pos[3];
	CharStat Insert;
	while (true)
	{
		//chọn 2 phần tử xuất hiện ít nhất (2 phần tử cuối ở bảng)
		//để thêm vào cây
		for (j = 1; j <= 2; j++)
		{
			//xét xem phần tử đang xét đã được thêm vào cây hay chưa
			for (i = 0, isAdded = false; i < Offset; i++)
			{
				if (HuffTree[i].c == (stat.end() - j)->name)
					if (HuffTree[i].nFreq == (stat.end() - j)->frequency)
					{
						isAdded = true;
						Pos[j] = i;				//ghi nhớ vị trí đã thêm vào
						break;
					}
			}
			//nếu phần tử đang xét chưa được thêm vào cây
			//thêm vào cây
			if (!isAdded)
			{
				HuffTree[Offset].c = (stat.end() - j)->name;
				HuffTree[Offset].nFreq = (stat.end() - j)->frequency;
				HuffTree[Offset].nLeft = HuffTree[Offset].nRight = -1;
				Pos[j] = Offset;			//ghi nhớ vị trí đã thêm vào
				Offset++;
			}
		}

		//thêm nút cha vào cây
		if (stat.size() == 2)
			Offset = 0;
		HuffTree[Offset].c = (stat.end() - 1)->name;
		HuffTree[Offset].nFreq = (stat.end() - 1)->frequency + (stat.end() - 2)->frequency;
		HuffTree[Offset].nLeft = Pos[1];
		HuffTree[Offset].nRight = Pos[2];
		Offset++;
		if (stat.size() == 2)
			break;

		//loại bỏ 2 phần tử ra khỏi bảng
		stat.erase(stat.end() - 1);
		stat.erase(stat.end() - 1);

		//thêm nút cha vào bảng
		Insert.name = HuffTree[Offset - 1].c;
		Insert.frequency = HuffTree[Offset - 1].nFreq;
		for (vector<CharStat>::iterator i = stat.begin(); i != stat.end(); i++)
		{
			//nếu 2 nút có trọng số bằng nhau
			if (i->frequency == Insert.frequency)
			{
				//nếu nút cần thêm có mã ACSII nhỏ nhất
				//thì thêm vào bảng
				if (Insert.name < i->name)
				{
					stat.insert(i, Insert);
					break;
				}
			}
			//nếu nút cần thêm có trọng số nhỏ hơn
			//thì thêm vào bảng
			else if (Insert.frequency > i->frequency)
			{
				stat.insert(i, Insert);
				break;
			}
		}
	}

	return HuffTree;
}

/***************************************************************
*	Tên hàm		:	generateCodeList
*	Chức năng	:	dựa vào cây Huffman để tạo một danh sách mã 
*					bit tương ứng với các ký tự
*	Truyền vào	:	node gốc của cây Huffman, con trỏ tới
*					danh sách mã bit cần tạo
*	Trả về		:	không
*	Người viết	:	Duy
***************************************************************/
void generateCodeList(HUFFNode *root, symbol *code_list)
{
	/* Chưa có... */
}

/***************************************************************
*	Tên hàm		:	compressFile
*	Chức năng	:	dịch đoạn bit gốc của file thành đoạn bit nén
*					Huffman, sau đó ghi đoạn bit này ra file đích
*					đồng thời cập nhật số bit align
*	Truyền vào	:	node gốc của cây Huffman, tên file cần nén,
*					tên file nén, số bit align
*	Trả về		:	kích thước file sau khi nén
*	Người viết	:	Duy
***************************************************************/
unsigned int compressFile(HUFFNode *root, char *inFilename, 
	char *outFilename, int& align) {
	symbol code_list[NUM_CHARS];
	FILE *iF, *oF;
	int c, i;
	int bit_count, byte_count;
	char bit_buffer;

	/* Reset số bit align */
	align = 0;

	/* Mở các file */
	if ((iF = fopen(inFilename, "rb")) == NULL) {
		exit(EXIT_FAILURE);
	}

	if ((oF = fopen(outFilename, "wb")) == NULL) {
		exit(EXIT_FAILURE);
	}

	generateCodeList(root, code_list); /* Tạo bảng mã */

	bit_buffer = 0;
	bit_count = 0;
	byte_count = 0;

	/* Ghi các mã bit đã encode ra file từng byte một */
	while ((c = fgetc(iF)) != EOF) {
		for (i = 0; i < code_list[c].length; i++) {
			bit_count++;
			/* Ghép các bit từ danh sách mã bit nối tiếp nhau để lấp đầy buffer */
			bit_buffer = (bit_buffer << 1) | (testBit(code_list[c].code, i) == 1);

			if (bit_count == 8) {
				/* Ngay khi buffer vừa đầy thì ghi buffer ra file */
				fputc(bit_buffer, oF);
				byte_count++;
				bit_count = 0;
			}
		}
	}
	/* Số bit trong buffer không đủ 1 byte -> đã đi đến cuối danh sách mã bit */
	/* Ghi các bit còn lại, bao gồm luôn 1 số bit offset (align) */
	if (bit_count != 0) {
		align = 8 - bit_count;
		bit_buffer <<= 8 - bit_count;
		fputc(bit_buffer, oF);
		byte_count++;
	}
	return byte_count;
}
