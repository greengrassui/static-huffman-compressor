﻿#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include "bitops.h"

using namespace std;

#define NUM_CHARS 256

/* Node cây Huffman */
typedef struct {
	char c;
	long nFreq;
	int nLeft;
	int nRight;
} HUFFNode;

struct CharStat {
	char name;
	unsigned int frequency;
};

HUFFNode* HuffmanTree(vector<CharStat> stat);

/* Các error code */
#define ERR_FILE_NOT_FOUND 404


/* Phần tử danh sách mã bit */
typedef struct symbol
{
	unsigned char length;     /* độ dài mã bit */
	unsigned char code[32];    /* mã bit của ký tự */
} symbol;

/* Các hàm liên quan đến thao tác trên file */
unsigned int compressFile(HUFFNode *root, char *inFilename, char *outFilename, int &align);