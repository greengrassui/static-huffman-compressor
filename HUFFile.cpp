#include "HUFFile.h"

HUF::HUF(void)
{
	m_CodeFile[0] = 'G'; m_CodeFile[1] = '3';
	m_nCode = 0;
}

HUF::HUF(char * Code, unsigned char nCode, vector<CharStat> CodeTable)
{
	m_CodeFile[0] = Code[0]; m_CodeFile[1] = Code[1];
	m_nCode = nCode;
	m_CodeTable = CodeTable;
}

HUF::~HUF(void)
{
}

bool HUF::isHUF()
{
	return m_CodeFile[0]=='G'&&m_CodeFile[1]=='3';
}

HUFHeader HUF::getHeader(int i)
{
	return m_FileHeader[i];
}

void HUF::insertHeader(HUFHeader t)
{
	m_FileHeader.push_back(t);
}

istream& operator>>(istream& in, HUF& info)
{
	if (in)
	{
		in.read(info.m_CodeFile, 2);
		char temp;
		in.read(&temp, 1);
		info.m_nCode = temp & 0x00ff;
		for (int i = 0; i < info.m_nCode; i++)
		{
			CharStat tempCS;
			char tempT[5];
			in.read(tempT, 5);
			tempCS.name = tempT[0];
			tempCS.frequency = 0;
			for (int i = 1; i < 5; i++)
			{
				tempCS.frequency = tempCS.frequency << 8;
				tempCS.frequency = tempCS.frequency | tempT[i];
			}
			info.m_CodeTable.push_back(tempCS);
		}

		while (!in.eof())
		{
			HUFHeader tempHeader;
			bool t = tempHeader.Input(in);
			if (t == false) break;
			info.insertHeader(tempHeader);
			in.seekg(tempHeader.nextFileAddrOffset);
		}
	}
	return in;
}

ostream& operator<<(ostream& out, HUF& info)
{
	if (out)
	{
		out.write(info.m_CodeFile, 2);
		char temp = info.m_nCode & 0xff;
		out.write(&temp, 1);
		for (int i = 0; i < info.m_nCode; i++)
		{
			char tempCS[5];
			tempCS[0] = info.m_CodeTable[i].name;
			tempCS[1] = info.m_CodeTable[i].frequency >> 24 & 0xff;
			tempCS[2] = info.m_CodeTable[i].frequency >> 16 & 0xff;
			tempCS[3] = info.m_CodeTable[i].frequency >> 8 & 0xff;
			tempCS[4] = info.m_CodeTable[i].frequency & 0xff;
			out.write(tempCS, 5);
		}
	}
	return out;
}

bool HUFHeader::Input(istream& in)
{
	fileName = "";
	char temp;
	temp = in.get();
	while (temp<8 && temp>-1)
	{
		fileName += temp;
		in.get();
	}
	alignBits = temp;

	char tempChar[4];
	in.read(tempChar, 4);
	sizeBefore = 0;
	for (int i = 0; i < 4; i++)
	{
		sizeBefore = sizeBefore << 8;
		sizeBefore = sizeBefore | tempChar[i];
	}

	in.read(tempChar, 4);
	nextFileAddrOffset = 0;
	for (int i = 0; i < 4; i++)
	{
		nextFileAddrOffset = nextFileAddrOffset << 8;
		nextFileAddrOffset = nextFileAddrOffset | tempChar[i];
	}
	curFileAddrOffset = (unsigned int)in.tellg();
	if (curFileAddrOffset == -1) return false;
	sizeAfter = nextFileAddrOffset - curFileAddrOffset;
	return true;
}

bool HUFHeader::Output(ostream & out)
{
	int n = fileName.length();
	int i = 0;
	while (i<n)
	{
		out.write(&fileName[i], 1);
	}
	out.write(&alignBits, 1);

	char tempChar[4];
	tempChar[0] = sizeBefore >> 24 & 0xff;
	tempChar[1] = sizeBefore >> 16 & 0xff;
	tempChar[2] = sizeBefore >> 8 & 0xff;
	tempChar[3] = sizeBefore & 0xff;
	out.write(tempChar, 4);

	tempChar[0] = nextFileAddrOffset >> 24 & 0xff;
	tempChar[1] = nextFileAddrOffset >> 16 & 0xff;
	tempChar[2] = nextFileAddrOffset >> 8 & 0xff;
	tempChar[3] = nextFileAddrOffset & 0xff;
	out.write(tempChar, 4);
	return true;
}
