#include "Function.h"

void Encode(char * folder_in, char * file_out)
{
	ofstream out(file_out, ios::binary);
	if (out.is_open())
	{
		vector<fs::path> p = fileList(folder_in);
		int k = p.size();
		if (k > 0)
		{
			vector<CharStat> stat = CharFreq(p);
			if (stat.size() > 0)
			{
				HUF info("G3", (unsigned int)stat.size(), stat);
				out << info;
				//tao cay huffman
				HuffNode* root = HuffmanTree(stat);
				//Nen tung file
				
				for (int i = 0; i < k; i++)
				{
					HUFHeader infoFile;
					infoFile.curFileAddrOffset = (unsigned int)out.tellp();
					infoFile.fileName = p[i].string();
					ifstream inFile(infoFile.fileName);

					cout << "Nen file thu: " << i + 1 << "...\n";

					infoFile.sizeAfter = CompressFile(inFile, out, root, infoFile.alignBits);
					inFile.seekg(0, inFile.end);
					infoFile.sizeBefore = (unsigned int)inFile.tellg();
					infoFile.nextFileAddrOffset = (unsigned int)out.tellp();
					out.seekp(infoFile.curFileAddrOffset);
					infoFile.Output(out);
					out.seekp(infoFile.nextFileAddrOffset);
					info.insertHeader(infoFile);
				}
				out.close();
				cout << "Nen file thanh cong.\n";
			}
			else
			{
				cout << "Nen khong thanh cong.";
			}
		}
		else
		{
			cout << "Khong mo duoc thu muc.";
		}
	}
	else
	{
		cout << "Khong tao duoc file.";
	}
}

void View(char * file)
{
	ifstream in(file, ios::binary);
	if (in)
	{
		HUF info;
		in >> info;
		if (info.isHUF())
		{
			vector<HUFHeader> stat = info.getHeaders();
			int n = stat.size();
			cout << "Ten file\t\t\tSize truoc nen\tSize sau nen";
			for (int i = 0; i < n; i++)
			{
				cout << i + 1 << ". " << stat[i].fileName << "\t" << (unsigned int)stat[i].sizeBefore << "\t" << (unsigned int)stat[i].sizeAfter;
			}
		}
		else
		{
			cout << "File sai dinh dang hoac bi hu.";
		}
	}
	else
	{
		cout << "Mo file khong thanh cong.";
	}
}

bool Decode(char * func, char * file)
{
	if (func[0] == '-')
	{
		vector<int> n;
		if (func[1] == 'a'&&func[2] == 'l'&&func[3] == 'l'&&func[4] == '\0')
		{

		}
		else if (func[1] == '[')
		{
			int i = 2;
			while (func[i] != ']')
			{
				int temp = 0;
				while (func[i] != ',')
				{
					temp = temp * 10 + func[i] - 48;
					i++;
				}
				i++;
			}
		}
		else
		{
			return false;
		}

	}
	return false;
}
