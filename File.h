#ifndef __FILE_H__
#define __FILE_H__

#include <experimental\filesystem>
#include <fstream>
#include <vector>
#include <string>
using namespace std;
namespace fs = experimental::filesystem;

struct CharStat
{
	char name;
	unsigned int frequency;
};

vector<fs::path> fileList(char * directory);

vector<CharStat> CharFreq(vector<fs::path>& p);

#endif